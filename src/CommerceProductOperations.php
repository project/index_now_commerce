<?php

namespace Drupal\index_now_commerce;

use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\index_now\Service\IndexNowInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Commerce product operations.
 */
class CommerceProductOperations implements ContainerInjectionInterface, CommerceProductOperationsInterface {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * Node operations class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\index_now\Service\IndexNowInterface $indexNow
   *   The index now service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected IndexNowInterface $indexNow,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('index_now.indexnow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function pingIndexNow(Product $commerce_product, string $event): void {
    if ($this->isIndexable($commerce_product, $event)) {
      $route_parameters = [
        'commerce_product' => $commerce_product->id(),
      ];
      $options = [
        'absolute' => TRUE,
        'language' => $commerce_product->language(),
      ];
      $product_url = Url::fromRoute('entity.commerce_product.canonical', $route_parameters, $options);
      $this->indexNow->sendRequest($product_url->toString());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isIndexable(Product $commerce_product, string $event): bool {
    $anonymous_user = $this->userStorage->load(0);
    if (!$commerce_product->access('view', $anonymous_user)) {
      return FALSE;
    }

    $exclude_events = $this->configFactory
      ->getEditable('index_now.settings')
      ->get('exclude_commerce_product_events');

    if (isset($exclude_events[$event]) && !empty($exclude_events[$event])) {
      return FALSE;
    }

    $exclude_product_types = $this->configFactory
      ->getEditable('index_now.settings')
      ->get('exclude_commerce_product_types');
    $exclude_product_types = !is_array($exclude_product_types) ? [] : array_filter(array_values($exclude_product_types));

    return !in_array($commerce_product->bundle(), $exclude_product_types);
  }

}

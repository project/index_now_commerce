<?php

namespace Drupal\index_now_commerce;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\path_alias\Entity\PathAlias;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Path alias operations class.
 */
class PathAliasOperations implements ContainerInjectionInterface {

  /**
   * Node operations class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver service.
   * @param \Drupal\Core\Path\PathValidatorInterface $pathValidator
   *   The path validator service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ClassResolverInterface $classResolver,
    protected PathValidatorInterface $pathValidator,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('class_resolver'),
      $container->get('path.validator')
    );
  }

  /**
   * Ping Index Now for the given path alias.
   *
   * @param \Drupal\path_alias\Entity\PathAlias $path_alias
   *   The path alias we want to ping.
   */
  public function pingIndexNow(PathAlias $path_alias): void {
    $type_and_id = $this->getEntityTypeAndIdFromPath($path_alias->getPath());
    $entity_type = $type_and_id['entity_type'];
    $entity_id = $type_and_id['entity_id'];

    if ('commerce_product' !== $entity_type) {
      return;
    }

    $entity_storage = $this->entityTypeManager->getStorage($entity_type);
    $entity = $entity_storage->load($entity_id);
    if (!$entity) {
      return;
    }

    $this->classResolver
      ->getInstanceFromDefinition(CommerceProductOperations::class)
      ->pingIndexNow($entity, 'insert');
  }

  /**
   * Get the entity type and ID from a path.
   *
   * @param string $path
   *   The path we want to get the entity type and ID from.
   *
   * @return array
   *   An array containing the entity type and ID.
   */
  public function getEntityTypeAndIdFromPath(string $path): array {
    $result = ['entity_type' => NULL, 'entity_id' => NULL];
    $url = $this->pathValidator->getUrlIfValid($path);
    if ($url && $url->isRouted()) {
      $route_parameters = $url->getRouteParameters();
      foreach ($route_parameters as $entity_type => $id) {
        if (is_numeric($id) && $this->entityTypeManager->hasDefinition($entity_type)) {
          $result['entity_type'] = $entity_type;
          $result['entity_id'] = $id;
          break;
        }
      }
    }

    return $result;
  }

}

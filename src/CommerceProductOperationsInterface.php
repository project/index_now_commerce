<?php

namespace Drupal\index_now_commerce;

use Drupal\commerce_product\Entity\Product;

/**
 * Commerce product operations interface.
 */
interface CommerceProductOperationsInterface {

  /**
   * Acts on a commerce product operation for the allowed product types.
   *
   * @param \Drupal\commerce_product\Entity\Product $commerce_product
   *   The commerce product being inserted / updated / deleted.
   * @param string $event
   *   An 'insert', 'update' or 'delete' event.
   */
  public function pingIndexNow(Product $commerce_product, string $event): void;

  /**
   * Tells if a commerce product is indexable or not.
   *
   * @param \Drupal\commerce_product\Entity\Product $commerce_product
   *   The commerce product being inserted / updated / deleted.
   * @param string $event
   *   An 'insert', 'update' or 'delete' event.
   *
   * @return bool
   *   Return true is the commerce product type has not been excluded in the
   *   Index Now conf, if the commerce product is published and if the anonymous
   *   role has "access content" permission.
   */
  public function isIndexable(Product $commerce_product, string $event): bool;

}

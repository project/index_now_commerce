<?php

namespace Drupal\index_now_commerce\Alter\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Alter index_now_settings form.
 */
class ModuleSettingsFormAlter implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The index_now settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Existing commerce products types.
   *
   * @var array
   */
  protected $productsTypes;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->config = $configFactory->get('index_now.settings');
    $this->productsTypes = $entityTypeManager
      ->getStorage('commerce_product_type')
      ->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Alter the form.
   *
   * @param array $form
   *   The renderable form array we want to alter.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    $hooks_options = [
      'insert' => $this->t('Entity created'),
      'update' => $this->t('Entity updated'),
      'delete' => $this->t('Entity deleted'),
    ];

    if (!isset($form['tabs'])) {
      $form['tabs'] = [
        '#type' => 'vertical_tabs',
        '#title' => $this->t("Restrict use"),
      ];
    }

    $form['commerce_product'] = [
      '#type' => 'details',
      '#title' => $this->t("Products types"),
      '#group' => 'tabs',
    ];

    $products_types = [];
    /** @var \Drupal\commerce_product\Entity\ProductType $product_type */
    foreach ($this->productsTypes as $machine_name => $product_type) {
      $products_types[$machine_name] = $product_type->label();
    }

    $form['commerce_product']['exclude_commerce_product_types'] = [
      '#type' => 'checkboxes',
      '#options' => $products_types,
      '#title' => $this->t("Exclude products types"),
      '#description' => $this->t("Selected products types won't use Index now"),
      '#default_value' => $this->config
        ->get('exclude_commerce_product_types') ?? [],
    ];

    $form['commerce_product']['exclude_commerce_product_events'] = [
      '#type' => 'checkboxes',
      '#options' => $hooks_options,
      '#title' => $this->t("Exclude events"),
      '#description' => $this->t("Selected events won't use Index now"),
      '#default_value' => $this->config
        ->get('exclude_commerce_product_events') ?? [],
    ];

    $form['#submit'][] = [static::class, 'saveCommerceConfig'];
  }

  /**
   * Save commerce config.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function saveCommerceConfig(array &$form, FormStateInterface $form_state): void {
    $config = \Drupal::service('config.factory')->getEditable('index_now.settings');
    $config->set('exclude_commerce_product_types', $form_state->getValue('exclude_commerce_product_types'));
    $config->set('exclude_commerce_product_events', $form_state->getValue('exclude_commerce_product_events'));
    $config->save();
  }

}

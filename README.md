# Index Now Commerce


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

**Index Now Commerce** extends [Index Now](https://www.drupal.org/project/index_now)
module ^3.0.x and submit requests to search engines when commerce products has
been created, updated or deleted on your website.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/index_now_commerce

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/index_now_commerce


REQUIREMENTS
------------

This module requires [Index Now](https://www.drupal.org/project/index_now)
module and Commerce product which is part of the
[Commerce Core](https://www.drupal.org/project/commerce) module.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Please refer to the
[Index Now Configuration](https://www.drupal.org/project/index_now#module-config)
section and read how to configure 2.0.x/3.0.x version.


MAINTAINERS
-----------

Current maintainers:
 * Maxime Roux (MacSim) - https://www.drupal.org/u/macsim
